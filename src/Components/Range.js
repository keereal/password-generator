import React from "react";

const Range = (props) => {
  return <input className="app_range" onChange={props.onChange} value={props.range} type="range" min="6" max="22" />;
};

export default Range;
