import React, {useEffect} from "react";

const Display = (props) => {
    const {text} = props;
    useEffect(() => {
        document.querySelector('.app_display').addEventListener('click', function () {
            const input = this.lastElementChild;
            input.select();
            document.execCommand('copy');
            this.focus();
            setTimeout(() => {
                input.value = 'Copied to clipboard';
            }, 50);
            setTimeout(() => {
                input.value = text;
            }, 1000);
        })
    })
    return <div className="app_display">
        <i className="fa fa-lock" aria-hidden="true" />
        <input type="text" value={text} />
    </div>;
};

export default Display;
