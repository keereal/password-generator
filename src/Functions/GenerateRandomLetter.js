export default function generateRandomLetter(uppercase = false) {
    const randomLetters = "abcdefghijklmnopqrstuvwxyz".split("");
    const randomLetter =
        randomLetters[Math.floor(Math.random() * randomLetters.length)];
    return uppercase && Math.random() > 0.5
        ? randomLetter.toLocaleUpperCase()
        : randomLetter;
}
