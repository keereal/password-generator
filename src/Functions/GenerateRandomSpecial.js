export default function generateRandomSpecial() {
  const randomSpecials = "!@#$%^&*()_+-=;.,></'\"/".split("");
  return randomSpecials[Math.floor(Math.random() * randomSpecials.length)];
}
